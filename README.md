# Creating a Production-Ready Spring Project Deployed in a Kubernetes Cluster

<h3>Introduction:</h3>

In response to the assignment, I have successfully created a Spring-based application and deployed it into a production environment using Kubernetes. This write-up outlines the steps and decisions made during the process.

<h3>Application Development:</h3>

For the application development, I created a simple Spring project that echoes the standard message "Spring is here!" when accessed. The application has been successfully deployed and is accessible at the following URL: http://spring.goelengineering.com/

The project is placed in a version control system, specifically Git. This ensures that our code is tracked, versioned, and readily available for collaboration and future updates. The link to the project is : https://gitlab.com/evannoelchi/spring-webapp.git

Run the command <code>'mvn clean install'</code> to package the Spring project

NB: pom.xml should have the same JAVA version as the one on packaging machine and Dockerfile

<h3>Containerization with ECR:</h3>

To ensure portability and ease of deployment, I containerized our Spring application using Docker. A Dockerfile was created that contains the necessary instructions to build an image that can run and display our application. The Docker image is stored in ECR. By default, ECR repositories are private, ensuring that our container images are not accessible to the public internet. I can control who has access to the project repositories. ECR is fully integrated with Amazon Elastic Kubernetes Service (EKS) and other AWS services, making it a seamless choice for our solution.

The steps used to containarize tha application are as follows

- Build image, my local machine is a MacOS M1 hence the need tp specify the platform  <code>docker buildx build --platform linux/amd64 -t $IMAGE_NAME . </code>

- Login to ECR <code>aws ecr get-login-password --region $AWS_DEFAULT_REGION | docker login --username AWS --password-stdin $ECR_REGISTRY_URL</code>

- Include tags on image <code>docker tag $IMAGE_NAME:$IMAGE_TAG $ECR_REGISTRY_URL/$IMAGE_NAME:$IMAGE_TAG</code>

- Lastly, push the image to ECR <code>docker push $ECR_REGISTRY_URL/$IMAGE_NAME:$IMAGE_TAG</code>

<h3>Deploy Agent (CICD):</h3>

GitLab CI/CD is a versatile and integrated solution that simplifies and automates the deployment of applications. GitLab CI/CD works seamlessly with Git version control, ensuring that the deployment processes are synchronized with our codebase. It is possible to define and manage CI/CD pipelines directly within our Git repository, which promotes versioning and collaboration. A t2.small EC2 instance was also intergrated with Gitlab CI to act as a Gitlab Runner. This EC2 instance was solely provisioned so as to assist the Gitlab CI to run the pipeline jobs without much interruption.

<h3>Deployment with Kubernetes:</h3>

Our application was deployed into a Kubernetes cluster, managed with Amazon Elastic Kubernetes Service (EKS), a production-ready container orchestration platform. We chose Kubernetes due to its popularity, scalability, and extensive community support. The deployment process was automated using Gitlab CI.

- For quick deployment the cluster was deployed with eksctl as shown below:

<code>eksctl create cluster --name $EKS_CLUSTER_NAME --nodes-min=2 --region $AWS_DEFAULT_REGION</code>

- ELK stack which is a powerful and popular open-source solution for log and data analytics was provisioned separately with yaml files <code>elastic-stack.yaml</code> and <code>fluentd-config.yaml</code> (also included in repo) once the cluster was up and running. 

<code>kubectl apply -f *.yaml</code>

The link to access Kibana - http://kibana.goelengineering.com:5601/

- Prometheus and Grafana, have also been provisioned to assist with monitoring and observability. Grafana will be used for dashboarding since it provides a user-friendly interface for querying, exploring, and visualizing data collected by Prometheus. Prometheus and Grafana will be deployed using helm charts <code>kube-prometheus-stack</code>

The following commands where used to install our Prometheus and Grafana.

<code>helm repo add prometheus-community https://prometheus-community.github.io/helm-charts</code>
<code>helm repo update</code>
<code>helm install [RELEASE_NAME] prometheus-community/kube-prometheus-stack</code>

The Links to access Prometheus and Grafana are given below

 Grafana - http://grafana.goelengineering.com/login, use the login details <code>username: 'admin' password: 'prom-operator'</code>
 Prometheus - http://prometheus.goelengineering.com/

- Installing Ingress Controller - this is a crucial component responsible for managing and controlling the external access to services within our cluster. To make use of the Ingress controller, firstly I had to deploy the Ingress Controller using helm running the command below:

<code>helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace</code>

Next I had to define Ingress resources in Kubernetes manifests <code>ingress.yaml</code>. These resources specify how external traffic should be routed to your services. 
After deploying the routing with <code>kubectl apply -f ingress.yaml</code> and testing then ingress controller should be working in our cluster.

<h3>Infrastructure Choice:</h3>

The infrastructure for this project was all deployed with AWS. The infrastruture included the EKS Cluster, Load Balancers, ECR, ACM, EC2 instance, VPCs, subnets, NACLs, internet gateways etc. Inorder to come up with a solution near production standard I realized that the free tier offering was not going to be sufficient and I was definetely going to incurr costs. Thankfully I had some credit available in my account from AWS and didn't have to make many compromises while working on the solution.

<h3>Conclusion</h3>

In conclusion my choices in working on this project were made based on industry best practices, scalability, and community support. I would love to discuss the details of the solution, including the technical aspects and decisions made. There are a number of improvements that can be made to the solution provided there is ample time. I have listed a few to be considered

- Provision all the resources with terraform and make use of terraform import, to import all the resources to a terraform state file that will be located in an S3 backend. I had to abandon this because of limited time, but it is recommended that infrastructure be provisioned and managed as IaC for reusability.

- Improve the Github CI pipeline and include stages to compile the spring application and build the docker image. I actually had included this in my pipeline but later discarded because of a bug indicated below in the build context.

<code>ERROR: failed to solve: failed to compute cache key: failed to calculate checksum of ref c011db04-afda-4f1f-9e23-63c979e54781::qjol1a0vlm90rn0383uq4yf80: failed to walk /var/lib/docker/tmp/buildkit-mount3882436052/target: lstat /var/lib/docker/tmp/buildkit-mount3882436052/target: no such file or directory.</code>

I did some troubleshooting on this and ended up abandoning the solution for now.

- Deploy the Gitlab runner in kubernetes cluster instead of running it on an EC2 instance.

- For monitoring & observabillity create Dashboards and also an alerting system that will send alerts to the rest of the team e.g alert messages send to slack or email.

<h3>Thank you !!!!</h3>







